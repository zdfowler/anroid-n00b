package com.example.echo.codelearntwitterapp.models;

/**
 * Class to hold strings that the app uses.
 */

public class TwitterAppConstants {
    public static final String SHARED_PREFS_COLLECTION_NAME = "codelearn_twitter";
    public static final String SHARED_PREFS_USERNAME_KEY = "saved_username_value";
    public static final String SHARED_PREFS_PASSWORD_KEY = "saved_password_value";
    public static final String SHARED_PREFS_TOKEN_KEY = "app_token";

    public static final String TWEETS_CACHE_FILES = "tweet_cache.ser";

    public static final String APP_LOGIN_SERVICE_URL = "https://app-dev-challenge-endpoint.herokuapp.com/login";

}
