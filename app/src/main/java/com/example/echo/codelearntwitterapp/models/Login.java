package com.example.echo.codelearntwitterapp.models;

import java.io.Serializable;

/**
 * Created by Echo on 9/15/2014.
 * Modified by ZDF
 */
public class Login implements Serializable {
    private String username;
    private String password;

    private static final long serialVersionUID = 1L;

    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
}
