package com.example.echo.codelearntwitterapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.example.echo.codelearntwitterapp.models.Login;
import com.example.echo.codelearntwitterapp.models.TwitterAppConstants;
import com.google.gson.Gson;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpParams;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;


public class MyActivity extends Activity {

    Button _loginBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);

        // Don't allow a screenshot on the "Recent" menu!
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_SECURE);

        // If token found, load up the list.
        if (checkToken()) {
            loadTweetList();
        }

        _loginBtn = (Button) findViewById(R.id.btn_login);


        _loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
        public void onClick(View v) {
                _loginBtn.setText(getString(R.string.btn_login_clicked_text));

                // Get username / password fields.
                EditText username_text = (EditText) findViewById(R.id.fld_username);
                String username = username_text.getText().toString();

                EditText password_field = (EditText) findViewById(R.id.fld_pwd);
                String password = password_field.getText().toString();

                Log.d("Codelearn", "Caught username " + username + " and password " + password);

                // Login.
                Login userLogin = new Login();
                userLogin.setUsername(username);
                userLogin.setPassword(password);

                // Pass a reference to MyActivity to login on construction so
                // we can use the shared preferences and throw up some Toast.
                // Better way?
                AsyncLogin login_Act = new AsyncLogin(MyActivity.this);

                // Do the login, passing in the data structure for Login.
                login_Act.execute(userLogin);


            }
        });

    }

    public void loadTweetList() {
        Intent intent = new Intent(MyActivity.this, TweetListActivity.class);
        startActivity(intent);
        finish();
    }

    public boolean checkToken() {
        boolean validToken = false;
        SharedPreferences prefs = getSharedPreferences(TwitterAppConstants.SHARED_PREFS_COLLECTION_NAME, MODE_PRIVATE);

        String token = prefs.getString(TwitterAppConstants.SHARED_PREFS_TOKEN_KEY,null);

        // Typically, you would put in a better check than just to see if the token is THERE.
        if (token != null) {
            validToken = true;
            Log.d("CodeLearn","Found valid token: " + token);
        } else {
            Log.d("CodeLearn","Token not found!");
        }
//        String savedUsername = prefs.getString(SHARED_PREFS_USERNAME_KEY, null);
//        String savedPassword = prefs.getString(SHARED_PREFS_PASSWORD_KEY, null);
        // Blank inputs store as non-null values.  They store as a blank string
//        if (savedUsername != null && savedPassword != "") {
//            Log.d("Codelearn","Attempting to skip act load.");
//            Log.d("Codelearn","Username is : **" + savedUsername + "** and password is : **" + savedPassword + "**");
//            Intent intent = new Intent(this, TweetListActivity.class);
//            startActivity(intent);
//            finish();
//        }

        return validToken;
    }

}
