package com.example.echo.codelearntwitterapp;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.echo.codelearntwitterapp.models.Tweet;
import com.example.echo.codelearntwitterapp.models.TwitterAppConstants;


import java.io.FileInputStream;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;


public class TweetListActivity extends ListActivity {

    private ArrayAdapter<Tweet> arrayAdapter;
    private List<Tweet> readTweets = new ArrayList<Tweet>();
    private List<Tweet> tweets = new ArrayList<Tweet>();

    public static final String TWEETS_CACHE_FILES = "tweet_cache.ser";


    public void refreshTweets(List<Tweet> tweets) {
        for (Tweet t : tweets) {
//            // What if we can use the ArrayAdapter directly?
//            arrayAdapter.insert(t, 0);
            readTweets.add(0,t);
        }
        renderTweets(readTweets);
//      // arrayAdapter.notifyDataSetChanged();
        // asyncwrite?  Not sure when to write this out yet.
    }

    public void renderTweets(List<Tweet> tweets) {
        Log.d("CodeLearn","Drawing Tweets entered Render Tweets");

        arrayAdapter = new TweetAdapter(this, tweets);
        setListAdapter(arrayAdapter);

        Log.d("CodeLearn","Drew tweets from Render");

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Not sure if this needs to be here at the end or not.
        setContentView(R.layout.activity_tweet_list);

        // Read tweets from the file.
        // (Deal with first time!?)
        Log.d("CodeLearn","Reading tweets from cache (and pause)");
        try {
            // Update tweets from file on first view.
            FileInputStream fis = openFileInput(TWEETS_CACHE_FILES);
            ObjectInputStream ois = new ObjectInputStream(fis);
            readTweets = (List<Tweet>) ois.readObject();
            ois.close();
            fis.close(); // should these be elsewhere?
            Log.d("Codelearn", "Read " + readTweets.size() + " tweets from file!");
        } catch (IOException e) {
            // If file does not exist, start with a blank tweet.
            Tweet waiting = new Tweet();
            waiting.setTitle("Welcome!");
            waiting.setBody("Please wait a moment while we set things up.");
            waiting.setId("00000000");

            readTweets.add(waiting);
        } catch (Exception e) {
            // log exceptions
            Log.e("CodeLearn",e.getMessage());
        }

        renderTweets(readTweets);
        // Attempt to go get / set tweets locally.
        AsyncFetchTweets a = new AsyncFetchTweets(this);
        a.execute();
    }
    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        TextView t = (TextView) v.findViewById(R.id.tweetTitle);
        t.setText(getString(R.string.tweet_view_tweet_header_clicked));

        Intent i = new Intent(this, TweetDetail.class);
        i.putExtra("tweet", arrayAdapter.getItem(position));
        // Note, using adapter here, since things in view might change compared to adapter's list.
        startActivity(i);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.tweet_list, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == R.id.action_refresh) {
            AsyncRefreshTweets a = new AsyncRefreshTweets(this);
            a.execute();

            return true;
        }

        if (id == R.id.action_logout) {
            // Clear out shared pref token, and then open up the main activity.

            // Prob should refactor this out...
            SharedPreferences prefs = getSharedPreferences(TwitterAppConstants.SHARED_PREFS_COLLECTION_NAME, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = prefs.edit();
            editor.remove(TwitterAppConstants.SHARED_PREFS_TOKEN_KEY);
            editor.apply();

            // Can this be generic?
            Intent homescreen = new Intent(TweetListActivity.this, MyActivity.class);
            homescreen.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(homescreen);
            this.finish();

        }
        return super.onOptionsItemSelected(item);
    }
}
