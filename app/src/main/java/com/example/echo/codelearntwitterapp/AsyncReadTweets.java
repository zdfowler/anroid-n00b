package com.example.echo.codelearntwitterapp;

import android.os.AsyncTask;
import android.util.Log;

import com.example.echo.codelearntwitterapp.models.Tweet;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Zac on 9/14/2014.
 * Modified to read in tweets from file as an async task.
 * Not used anymore, but not removed just yet.
 *
 */
public class AsyncReadTweets extends AsyncTask<Void,Void,List<Tweet>> {

    private TweetListActivity calling_activity;

    public AsyncReadTweets(TweetListActivity tl) {
        calling_activity = tl;
    }

    @Override
    protected List<Tweet> doInBackground(Void... voids) {
        List<Tweet> readTweets = new ArrayList<Tweet>();
        Log.d("CodeLearn","Reading tweets from cache");
        try {
            // open file, perf ops
            FileInputStream fis = calling_activity.openFileInput(TweetListActivity.TWEETS_CACHE_FILES);
            ObjectInputStream ois = new ObjectInputStream(fis);
            readTweets = (List<Tweet>) ois.readObject();
            ois.close();
            fis.close(); // should these be elsewhere?
            Log.d("Codelearn", "Read tweets!");
        } catch (Exception e) {
            // log exceptions
            Log.e("CodeLearn",e.getMessage());
        }
        return readTweets;
    }

    @Override
    protected void onPostExecute(List<Tweet> tweets) {
        calling_activity.renderTweets(tweets);
    }

}
