package com.example.echo.codelearntwitterapp;



import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.example.echo.codelearntwitterapp.models.Tweet;
import com.example.echo.codelearntwitterapp.models.TwitterAppConstants;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Echo on 9/14/2014.
 * Modified by ZDF
 */
public class AsyncWriteTweets extends AsyncTask<List<Tweet>,Void,Void> {

    TweetListActivity callingTweetListActivity;

    public AsyncWriteTweets(TweetListActivity tl) {
        callingTweetListActivity = tl;
    }

    @Override
    protected Void doInBackground(List<Tweet>... tweets) {
        Log.d("CodeLearn","Writing out tweets to file");
        try {
            List<Tweet> localTweets = tweets[0];

            // Delete file first needed?
            FileOutputStream fos = callingTweetListActivity.openFileOutput(TwitterAppConstants.TWEETS_CACHE_FILES, Context.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(localTweets);
            Log.d("CodeLearn","Wrote out " + localTweets.size() + " tweets in AsyncWriteTask.");
            oos.reset(); // With only one write, this may not be needed.
            // We are writing an array of objects to the output stream (one array)
            // We may revisit later to just loop across objects.
            oos.close();
            fos.close();


        } catch (IOException e) {
            Log.e("CodeLearn", "IO Exception in AsyncWriteTweeets: " + e.getMessage());
        } catch (Exception e) {
            Log.e("CodeLearn","Unknown: " + e.getMessage());
        }

        Log.d("CodeLearn","Done writing tweets to file");

        return null;
    }
}
