package com.example.echo.codelearntwitterapp;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.example.echo.codelearntwitterapp.models.Tweet;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;


/**
 * Created by Echo on 9/14/2014.
 * Updated by Zac!
 */
public class AsyncFetchTweets extends AsyncTask<Void,Void,List<Tweet>>{
    private TweetListActivity calling_activity;
    private List<Tweet> tweets = new ArrayList<Tweet>();


    public AsyncFetchTweets(TweetListActivity activity) {
        calling_activity = activity;
    }

    @Override
    protected List<Tweet> doInBackground(Void... voids){
        Log.d("CodeLearn","Creating random tweets. (waiting)");
        try {


            // Simullate the network call.
            Thread.sleep(5000);
           // Create random tweets; Placeholder for internet query.
            Random r = new Random();
            for (int i = 0; i < 20; i++ ) {
                int j = r.nextInt();
                Tweet tweet = new Tweet();
                tweet.setTitle("Tweet # " + j);
                tweet.setBody("This is a longer message and random text for Tweet # " + j);
                tweets.add(tweet);
            }

            // Setup and call a nested async task to write to a cache file.
            Log.d("CodeLearn","Calling the async write...");
            AsyncWriteTweets wt = new AsyncWriteTweets(calling_activity);
            wt.execute(tweets);

            Log.d("CodeLearn","Leaving AsyncFetch.");
        } catch (Exception e) {
            Log.e("CodeLearn","Exception in FetchTweets: " + e.getMessage());
        }
        return tweets;
    }

    @Override
    protected void onPostExecute(List<Tweet> tweets) {
        calling_activity.renderTweets(tweets);
    }

}
