package com.example.echo.codelearntwitterapp;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.echo.codelearntwitterapp.models.Tweet;

import java.util.List;


/**
 * Created by Echo on 9/13/2014.
 * Modfiied by Zac!
 */
public class TweetAdapter extends ArrayAdapter<Tweet> {

    private List<Tweet> ts;

    private LayoutInflater inflater;

    public TweetAdapter(Activity activity, List<Tweet> tweets) {
        super(activity, R.layout.row_tweet, tweets);
        ts = tweets;
        inflater = activity.getWindow().getLayoutInflater();
    }
    public TweetAdapter(Activity activity, String[] str) {
        super(activity, R.layout.row_tweet);
        inflater = activity.getWindow().getLayoutInflater();
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View row = inflater.inflate(R.layout.row_tweet, parent, false);
        TextView tt = (TextView) row.findViewById(R.id.tweetTitle);
        TextView tb = (TextView) row.findViewById(R.id.tweetBody);
//        TextView td = (TextView) v.findViewById(R.id.tweetDate);
        Tweet t = ts.get(position);

        tt.setText(t.getTitle());
        tb.setText(t.getBody());

        return row;
    }
}
