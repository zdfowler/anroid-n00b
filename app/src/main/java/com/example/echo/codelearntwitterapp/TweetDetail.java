package com.example.echo.codelearntwitterapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.echo.codelearntwitterapp.models.Tweet;
import com.example.echo.codelearntwitterapp.models.TwitterAppConstants;


public class TweetDetail extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tweet_detail);

        Tweet tweet = (Tweet) getIntent().getSerializableExtra("tweet");
        TextView td_title = (TextView) findViewById(R.id.tweetTitle);
        TextView td_body = (TextView) findViewById(R.id.tweetBody);

        td_body.setText(tweet.getBody());
        td_title.setText(tweet.getTitle());

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }


        if (id == R.id.action_logout) {
            // Clear out shared pref token, and then open up the main activity.

            // Prob should refactor this out...
            SharedPreferences prefs = getSharedPreferences(TwitterAppConstants.SHARED_PREFS_COLLECTION_NAME, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = prefs.edit();
            editor.remove(TwitterAppConstants.SHARED_PREFS_TOKEN_KEY);
            editor.apply();

            // Can this be generic?
            Intent homescreen = new Intent(TweetDetail.this, MyActivity.class);
            homescreen.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(homescreen);
            this.finish();

        }

        return super.onOptionsItemSelected(item);
    }
}
