package com.example.echo.codelearntwitterapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.example.echo.codelearntwitterapp.models.Login;
import com.example.echo.codelearntwitterapp.models.TwitterAppConstants;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Echo on 9/14/2014.
 */
public class AsyncLogin extends AsyncTask<Login, Void, Boolean> {


    private MyActivity calling_activity;

    public AsyncLogin(MyActivity activity) {
        calling_activity = activity;
    }

    @Override
    protected Boolean doInBackground(Login... login) {

//        // Package GSON data
//        List<NameValuePair> data = new ArrayList<NameValuePair>(2);
//        data.add(new BasicNameValuePair("username", username));
//        data.add(new BasicNameValuePair("password", password));
//            StringBuffer stringBuffer = new StringBuffer();
//            post.setEntity(new UrlEncodedFormEntity(data));

        // Store token as shared prefs
        String usernameFromForm = login[0].getUsername();
        if (usernameFromForm.equals("zac")) {
            String passwordFromForm = login[0].getPassword();
            SharedPreferences prefs = calling_activity.getSharedPreferences(TwitterAppConstants.SHARED_PREFS_COLLECTION_NAME, Context.MODE_MULTI_PROCESS);
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString(TwitterAppConstants.SHARED_PREFS_USERNAME_KEY,usernameFromForm);
            editor.putString(TwitterAppConstants.SHARED_PREFS_PASSWORD_KEY,passwordFromForm);
            editor.apply();
            Log.d("CodeLearn","Username was zac, writing out shared prefes.");
//            return true;
        } else {
//            return false;
        }




        Log.d("CodeLearn", "Rcvd user: " + login[0].getUsername() + ", pass: " + login[0].getPassword());


        // POST to URL
        Log.d("CodeLearn", "Entering network call");
        try {

            AndroidHttpClient ahc = AndroidHttpClient.newInstance("CodeLearnTwitterApp");
            HttpPost post = new HttpPost(TwitterAppConstants.APP_LOGIN_SERVICE_URL);

            // Untested, new skill - using GSON
            String json = new GsonBuilder().create().toJson(login[0],Login.class);
            Log.d("CodeLearn","Json string: " + json);
            post.setEntity(new StringEntity(json));
            post.setHeader("Accept", "application/json");
            post.setHeader("Content-type", "application/json");

            HttpResponse response = ahc.execute(post);

            // Catch response
            StringBuffer stringBuffer = new StringBuffer();
            InputStream inputStream = response.getEntity().getContent();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream), 1024);
            String readLine = bufferedReader.readLine();
            while (readLine != null) {
                stringBuffer.append(readLine);
                readLine = bufferedReader.readLine();
            }
            ahc.close();


            // Check for "token"
            JSONObject jso = new JSONObject(String.valueOf(stringBuffer));
            String token = null;
            if (! jso.isNull("token")) {
                token = jso.getString("token");
                // Store token
                SharedPreferences prefs = calling_activity.getSharedPreferences(TwitterAppConstants.SHARED_PREFS_COLLECTION_NAME, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString(TwitterAppConstants.SHARED_PREFS_TOKEN_KEY,token);
                editor.apply();

                return true;
            } else {
                // No login.
                return false;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        return false;
    }

    @Override
    protected void onPostExecute(Boolean success) {
        if (success) {
            calling_activity.loadTweetList();
        } else {
            Toast.makeText(calling_activity.getApplicationContext(),"Login failed",Toast.LENGTH_LONG).show();
        }
    }

}
