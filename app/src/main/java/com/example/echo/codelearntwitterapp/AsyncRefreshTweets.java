package com.example.echo.codelearntwitterapp;

import android.os.AsyncTask;

import com.example.echo.codelearntwitterapp.models.Tweet;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Zac on 10/7/2014.
 */
public class AsyncRefreshTweets extends AsyncTask<Void, Void, List<Tweet>> {

    private TweetListActivity callingTweetListActivity;

    public AsyncRefreshTweets(TweetListActivity tl) {
        callingTweetListActivity = tl;
    }

    @Override
    protected List<Tweet> doInBackground(Void... voids) {
        Tweet t = new Tweet();
        t.setTitle("New Tweet!");
        t.setBody("New body!");
        t.setId("245234522");

        List<Tweet> tweets = new ArrayList<Tweet>();
        tweets.add(t);

        Tweet t2 = new Tweet();
        t2.setTitle("Second TWeet!");
        t2.setBody("Boooo!");
        t2.setId("23424112");
        tweets.add(t2);
        return tweets;
    }

    @Override
    protected void onPostExecute(List<Tweet> tweets) {
        callingTweetListActivity.refreshTweets(tweets);
    }
}